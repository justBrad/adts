#include "StackV.h"
#include <stdexcept>

int Stack::size(){
	return data.size();
}

void Stack::push(int val){
	data.push_back(val);
}

void Stack::pop(){
	data.pop_back();
}

int Stack::top(){
	if (data.size()>0){
		return data[data.size()-1];
	}
	else
		throw out_of_range("Out of range error");
}

void Stack::clear(){
	data.clear();
}