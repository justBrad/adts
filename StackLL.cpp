#include "StackLL.h"
#include <iostream>
class Stack::Node{
	public:
		int data = 0;
		Node* link = nullptr;
};

Stack::~Stack(){
	while(num_elements > 0)
		pop();
} //d'tor

int Stack::size(){
	return num_elements;
}

void Stack::push(int val){
	Node* newPtr = new Node{val};
	if (num_elements<1){
		frontPtr = newPtr;
	}
	else{
		Node* tmpPtr = frontPtr;
		while(tmpPtr->link != nullptr){
			tmpPtr = tmpPtr->link;
		}
		tmpPtr->link = newPtr;
	}
	num_elements++;
}

void Stack::pop(){
	Node* tmpPtr = frontPtr;
	Node* delPtr = tmpPtr->link;
	if (delPtr == nullptr){
		delPtr = tmpPtr;
	}
	else {
		while (delPtr->link != nullptr){
			tmpPtr = delPtr;
			delPtr = tmpPtr->link;
		}
	}
	tmpPtr->link = nullptr;
	delete delPtr;
	num_elements--;
}

int Stack::top(){
	Node* tmpPtr = frontPtr;
	while (tmpPtr->link != nullptr){
		tmpPtr = tmpPtr->link;
	}
	return tmpPtr->data;
}

void Stack::clear(){
	while(num_elements > 0)
		pop();
}